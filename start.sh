killall node

cd /root/microservices-ecommerce-master/user-authorization
nohup node server.js > output.log &

cd /root/microservices-ecommerce-master/product-hub
nohup node server.js > output.log &

cd /root/microservices-ecommerce-master/ordering-service
nohup node server.js > output.log &

cd /root/microservices-ecommerce-master/recommendation
nohup node server.js > output.log &

cd /root/microservices-ecommerce-master/client-app
nohup node index.js > output.log &
