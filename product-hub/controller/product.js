require('dotenv').config();
const Product = require('../models/').Product;
const User = require('../models/').User;
const Rate = require('../models/').Rate;
const Transaction = require('../models/').Transaction;
const sequelize = require('../models/').sequelize;
const Op = require('sequelize').Op;

const router = require('express').Router();
const joi = require('joi');
const validate = require('express-validation');
const isLoggedIn = require('../middleware/login');


router.get('/', async (req, res) => {
    try {
        const limit = Number(req.query.limit || 12);
        var offset = 0;
        
        if (req.query.page)
            offset = (req.query.page - 1) * limit;

        const data = await Product.findAndCountAll({
            limit,
            offset,
            include: [
                {
                  model: User,
                  attributes: ['id', 'nama'],
                  where: {
                    id: sequelize.col('product.userId')
                  },
                  as: 'seller'
                }
            ],
            order: [['id', 'DESC'], ['nama', 'ASC']]
        });

        res.json({ isOk: true, data });
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ err, isError: true });
    }
});

router.get('/search', async (req, res) => {
    try {
        var category = req.query.category;
        var query = req.query.query;

        const limit = Number(req.query.limit || 12);
        var offset = 0;
        
        if (req.query.page)
            offset = (req.query.page - 1) * limit;

        var data = null;
        if (category === 'All') {
            data = await Product.findAndCountAll({
                limit,
                offset,
                where: {  nama: { [Op.like]: `%${query}%` } },
                include: [{
                    model: User,
                    attributes: ['id', 'nama'],
                    where: {
                    id: sequelize.col('product.userId')
                    },
                    as: 'seller'
                }]
            });
        }
        else {
            data = await Product.findAndCountAll({
                limit,
                offset,
                where: {  nama: { [Op.like]: `%${query}%` }, category: category },
                include: [{
                    model: User,
                    attributes: ['id', 'nama'],
                    where: {
                    id: sequelize.col('product.userId')
                    },
                    as: 'seller'
                }]
            });
        }

        res.json({ isOk: true, data });
    }
    catch(err) {
        console.log(err);
        res.status(500).json({ err, isError: true });
    }
});

router.get('/product-popular', async (req, res) => {
    try {
        const limit = 10;
        const offset = 0;

        var data = await Product.findAll({
            limit,
            offset,
            include: [
                {
                    model: User,
                    attributes: ['id', 'nama'],
                    where: {
                    id: sequelize.col('product.userId')
                    },
                    as: 'seller'
                }
            ],
            order: [['id', 'DESC'], ['nama', 'ASC']]
        });

        data = [data, data];
        res.json({ isOk: true, data });
    }
    catch(err) {
        res.status(500).json({ isError: true, err });
    }
});

router.get('/own', isLoggedIn, async (req, res) => {
    try {
        const data = await Product.findAndCountAll({
            where: {
                userId: req.stateId
            }
        });

        res.json({ isOk: true, data });
    }
    catch (err) {
        res.json({ isError: true, err });
        console.error(err);
    }
});

router.post(
    '/create',
    isLoggedIn,
    validate({
        body: {
            nama: joi.string().required(),
            harga: joi.string().required(),
            stock: joi.string().required(),
            photoUrl: joi.string().required(),
            category: joi.string().required()
        },
        options: {
            allowUnknownBody: false
        }
    }),
    async (req, res) => {
        try {
            const body = req.body;
            const data = await Product.create({ ...body, userId: req.stateId });

            res.json({ isOk: true, data });
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ err, isError: true });
        }
    }
);

router.get('/:id', async (req, res) => {
    try {
        const id = Number(req.params.id);
        const data = await Product.findOne({
            where: {
                id
            }
        });

        if (data)
            res.json({ isOk: true, data });
        else
            res.status(404).json({ isError: true, errorMsg: 'Product Not Found' });
    }
    catch (err) {
        console.error(err);
        res.status(500).json({ err, isError: true });
    }
});

router.put(
    '/:id/edit',
    isLoggedIn,
    validate({
        params: {
            id: joi.number().required()
        },
        body: {
            nama: joi.string().required(),
            harga: joi.string().required(),
            stock: joi.string().required(),
            photoUrl: joi.string(),
            category: joi.string().required()
        },
        options: {
            allowUnknownBody: false
        }
    }),
    async (req, res) => {
        try {
            const productId = Number(req.params.id);
            const userId = Number(req.stateId);

            const isProductValid = await Product;
            const data = await Product.findOne({ where: { id: productId, userId } });
            // console.log('Edit One Product', data);

            if (!data)
                return res
                    .status(404)
                    .json({ isError: true, errorMsg: 'Product Not Found' });
            else {
                const body = req.body;
                const data = await Product.update(body, {
                    where: {
                        id: productId
                    }
                });

                res.json({ isOk: true, data });
            }
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ err, isError: true });
        }
    }
);

router.delete(
    '/:id/delete',
    isLoggedIn,
    validate({
        params: {
            id: joi.number().required()
        }
    }),
    async (req, res) => {
        try {
            const validate = Product.findOne({
                where: {
                    id: req.params.id,
                    userId: Number(req.stateId)
                }
            });

            if (!validate)
                return res
                    .status(404)
                    .json({ isError: true, errorMsg: 'Product Not Found' });

            const data = await Product.destroy({
                where: {
                    id: req.params.id,
                    userId: Number(req.stateId)
                }
            });

            res.json({ isOk: true, data });
        }
        catch (error) {
            console.error(err);
            res.json({ isError: true, err });
        }
    }
)

router.post(
    '/rate',
    isLoggedIn,
    validate({
        body: {
            productId: joi.number().required(),
            rate: joi.string().required(),
        },
        options: {
            allowUnknownBody: false
        }
    }),
    async (req, res) => {
        try {
            const body = req.body
            const userId = req.stateId;

            var rateData = {
                userId: userId,
                productId: body.productId,
                rate: body.rate
            };

            var rate = await Rate.findOne({ where: {productId: body.productId, userId: userId } });
            var data = null;

            if(rate) { // update
                data = await rate.update({ rate: body.rate });
            }
            else { // insert
                var rateData = {
                    userId: userId,
                    productId: body.productId,
                    rate: body.rate
                };

                data = await Rate.create(rateData);
            }

            var productRates = await Rate.findAll({ where: { productId: body.productId } });

            var sum = 0;
            for (var i = 0; i < productRates.length; i++) {
                var rate = productRates[i];
                sum = sum + parseInt(rate.rate, 10);
            }
            
            var newProductRate = Math.round(sum / productRates.length);
            var product = await Product.findOne({ where: {id: body.productId } });
            if(product) { // update
                await product.update({ rate: String(newProductRate) });
            }

            res.json({ isOk: true, newRate: String(newProductRate), data: data });
        }
        catch (err) {
            console.error(err);
            res.status(500).json({ err, isError: true });
        }
});

module.exports = router;
