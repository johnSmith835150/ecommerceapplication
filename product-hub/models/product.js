module.exports = async (db, Sequelize) =>
  db.define('product', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nama: Sequelize.STRING,
    harga: Sequelize.STRING,
    stock: Sequelize.INTEGER,
    userId: Sequelize.INTEGER,
    photoUrl: Sequelize.STRING,

    category: Sequelize.STRING,
    rate: Sequelize.ENUM('0', '1', '2', '3', '4', '5')
  }, {
    timestamps: false
  })
