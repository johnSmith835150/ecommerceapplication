require('dotenv').config()
const Sequelize = require('sequelize')
// const productModel = require('./product')
// const userModel = require('./user')
const transactionModel = require('./transaction')
var exports = module.exports = {};

const db = new Sequelize({
    host: process.env.HOST,
    username: process.env.DB_ATTR,
    password: process.env.DB_PASSWORD,
    database: process.env.DATABASE,
    dialect: 'mysql'
})

transactionModel(db, Sequelize)

const User = db.define(
    'user',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nama: Sequelize.STRING,
        email: {
            type: Sequelize.STRING,
            unique: true
        },
        password: Sequelize.STRING,
        balance: Sequelize.STRING,
        photoUrl: Sequelize.STRING,

        age: Sequelize.INTEGER,
        country: Sequelize.STRING,
        gender: Sequelize.STRING
    },
    {
        timestamps: false
    }
)

const Product = db.define(
    'product',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nama: Sequelize.STRING,
        harga: Sequelize.STRING,
        stock: Sequelize.INTEGER,
        userId: Sequelize.INTEGER,
        photoUrl: Sequelize.STRING,

        category: Sequelize.STRING,
        rate: Sequelize.ENUM('0', '1', '2', '3', '4', '5')
    },
    {
        timestamps: false
    }
)

const Rate =  db.define(
    'rate', 
    {
      id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
      },
        userId: Sequelize.INTEGER,
        productId: Sequelize.INTEGER,
        rate: Sequelize.ENUM('0', '1', '2', '3', '4', '5')
    }, 
    {
        timestamps: false
    }
)


User.hasMany(Product, {
    foreignKey: 'userId',
    as: 'seller'
})

Product.belongsTo(User, {
    foreignKey: 'userId',
    as: 'seller'
})
/// Setting Rate Foreign keys .. need as ???
User.hasMany(Rate,{
    foreignKey: 'userId',
    // as: 
})

Product.hasMany(Rate,{
    foreignKey: 'productId'
})

Rate.belongsTo(User,{
    foreignKey:'userId'
})

Rate.belongsTo(Product,{
    foreignKey:'productId'
})

Product.hasMany(db.models.transaction, {
    foreignKey: 'productId'
})

db.models.transaction.belongsTo(Product, {
    foreignKey: 'productId'
})

exports.getUserById = async (userId) => {
    try {
        const id = Number(userId);
        var data = await User.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getProductById = async (productId) => {
    try {
        const id = Number(productId);
        var data = await Product.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'Product Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getLatestProduct = async () => {
    try {
        var data = await exports.Product.findOne({
            order: [['id', 'DESC']],
            attributes: {
                exclude: ['photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'Product Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.sync = args => db.sync(args);
exports.sequelize = db;
exports.Product = db.models.product;
exports.User = db.models.user;
exports.Rate = db.models.rate;
exports.Transaction = db.models.transaction;
