require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
const { Kafka } = require('kafkajs');

var exports = module.exports = {};

const db = require('./models/');
const router = require('./controller/product');

const app = express();

var exports = module.exports = {};
exports.kafka = null;
exports.producer = null;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/** 
 * "/" -> get all products
 * "/product-popular" -> get most 3 popular products
 * "/own" -> get all current user products
 * "/create" (post) -> create new product
 * "/:id" -> view product with productId=id
 * "/:id/edit" -> update product with productId=id
 * "/:id/delete" -> delete product with productId=id
 * "/rate" -> rate product
 * "/search" -> search products
 */
app.use(morgan(async (tokens, req , res) => {
    var userId = 0;
    const auth = req.headers.authorization.split(' ')[1];
    try {
        const { id } = jwt.verify(auth, process.env.SECRET);
        userId = id;
    } catch (err) {
        console.log("samir cute!!! " + err);
    }

    var method = tokens.method(req , res);
    var url = tokens.url(req , res);
    var response = tokens.res(req, res, 'content-length');
    var status = tokens.status(req, res);

    var time = new Date();
    const timestamp = `${time.getFullYear()}-${time.getMonth()+1}-${time.getDate()} ${time.getHours()+1}:${time.getMinutes()}:${time.getSeconds()}`;

    var log = {
        method: method,
        url: url,
        status: status,
        userId: userId,
        productId: 0,
        timestamp: timestamp,
        user: {},
		product: {}
    };

    if (userId !== 0)
        log.user = await db.getUserById(userId);

    if (log.url.split('?')[0] == "/search")
        log.event = "search";
    else {
        var parsedUrl = log.url.split('/');
        if (parsedUrl.length == 3) {
            log.product = await db.getProductById(parsedUrl[1]);
            log.productId = Number(parsedUrl[1]);
            if (parsedUrl[2] === 'edit')
                log.event = "updateProduct";
            else if (parsedUrl[2] === 'delete')
                log.event = "deleteProduct";
            log.event = "unknown";
        }
        else {
            switch(log.url.split('?')[0]) {
                case "/":
                    log.event = "getAllProducts";
                    break;
                case "/product-popular":
                    log.event = "getPopularProducts";
                    break;
                case "/own":
                    log.event = "getCurrentUserProducts";
                    break;
                case "/create":
                    log.event = "createNewProduct";
                    break;
                case "/rate":
                    log.event = "rateProduct";
                    break;
                default:
                    log.event = "unknown";
                    break;
            };
    
            if ((log.url !== "/" && !isNaN(parsedUrl[1])) || log.url === '/rate') {
                var productId = null;
                if (log.url === '/rate')
                    productId = req.body.productId;
                else
                    productId = parsedUrl[1];
    
                log.productId = Number(productId);
                log.product = await db.getProductById(productId);
            }
    
            if (log.url !== "/" && !isNaN(parsedUrl[1]))
                log.event = "viewProduct";

            if (log.event === "createNewProduct")
                log.product = await db.getLatestProduct();
        }
    }

    await exports.producer.connect();
    await exports.producer.send({
        topic: process.env.KAFKA_TOPIC,
        messages: [ { value: JSON.stringify(log) } ],
    });

    return [
        method,
        url,
        status,
        response, '-',
        tokens['response-time'](req, res), 'ms'
    ].join(' ');

}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Method', 'GET, POST, PUT, OPTIONS');
    next();
});
app.use('/', router);

db.sync();

app.listen(process.env.PORT, () => {
    var brokers = process.env.BROKERS.split(' ');
    exports.kafka = new Kafka({
        clientId: 'product-hub',
        brokers: brokers
    });
    exports.producer = exports.kafka.producer();
    
    console.log(`Product hub running on port ${process.env.PORT}`);
});
