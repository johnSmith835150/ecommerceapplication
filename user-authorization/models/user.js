module.exports = async (db, Sequelize) =>
  db.define(
    'user',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      nama: Sequelize.STRING,
      email: {
        type: Sequelize.STRING,
        unique: true
      },
      password: Sequelize.STRING,
      balance: Sequelize.STRING,
      photoUrl: Sequelize.STRING,
      
      age: Sequelize.INTEGER,
      country: Sequelize.STRING,
      gender: Sequelize.STRING
    },
    {
      timestamps: false
    }
  )
