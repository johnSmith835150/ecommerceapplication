require('dotenv').config();
const Sequelize = require('sequelize');
const userModel = require('./user');

var exports = module.exports = {};

const db = new Sequelize({
	host: process.env.HOST,
	username: process.env.DB_ATTR,
	password: process.env.DB_PASSWORD,
	database: process.env.DATABASE,
	dialect: 'mysql'
});

exports.getUserById = async (userId) => {
    try {
        const id = Number(userId);
        var data = await exports.User.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getUserByEmail = async (email) => {
    try {
        var data = await exports.User.findOne({
            where: {
                email: email
            },
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getLatestUser = async () => {
    try {
        var data = await exports.User.findOne({
            order: [['id', 'DESC']],
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

userModel(db, Sequelize);

exports.sync = args => db.sync(args);
exports.sequelize = db;
exports.User = db.models.user;
