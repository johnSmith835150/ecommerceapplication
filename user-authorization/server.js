require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
const { Kafka } = require('kafkajs');

var exports = module.exports = {};

const db = require('./models/');
const router = require('./controller/user');

const app = express();

var exports = module.exports = {};
exports.kafka = null;
exports.producer = null;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/** 
 * "/login" -> userLogin
 * "/register" -> Signup
 * "/profile" -> viewProfile
 * "/edit" -> Update User
 * "/logout" -> userLogout
 */
app.use(morgan(async (tokens, req, res) => {
	var userId = 0;
	const auth = req.headers.authorization.split(' ')[1];
	try {
		const { id } = jwt.verify(auth, process.env.SECRET);
		userId = id;
	} catch (err) {
		console.log("samir cute!!! " + err);
	}

	var method = tokens.method(req, res);
	var url = tokens.url(req, res);
	var response = tokens.res(req, res, 'content-length');
	var status = tokens.status(req, res);

	var time = new Date();
	const timestamp = `${time.getFullYear()}-${time.getMonth()+1}-${time.getDate()} ${time.getHours()+1}:${time.getMinutes()}:${time.getSeconds()}`;
	
	var log = {
		method: method,
		url: url,
		status: status,
		userId: userId,
		productId: 0,
		timestamp: timestamp,
		user: {},
		product: {}
	};

	if (userId !== 0)
		log.user = await db.getUserById(userId);

	switch(log.url) {
		case "/login":
			log.event = "login";
			break;
		case "/register":
			log.event = "register";
			break;
		case "/profile":
			log.event = "viewProfile";
			break;
		case "/edit":
			log.event = "updateProfile";
			break;
		case "/logout":
			log.event = "logout";
			break;
		default:
			log.event = "unknown";
			break;
	};

	if (log.event === "login" && status !== '404') {
		log.user = await db.getUserByEmail(req.body.email);
		log.userId = log.user.id;
	}

	if (log.event === "register")
		log.user = await db.getLatestUser();

	await exports.producer.connect();
    await exports.producer.send({
        topic: process.env.KAFKA_TOPIC,
        messages: [ { value: JSON.stringify(log) } ],
    });

	return [
		method,
		url,
		status,
		response, '-',
		tokens['response-time'](req, res), 'ms'
	].join(' ');

}));

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Method', 'GET, POST, PUT, OPTIONS');
	next();
});
app.use('/', router);

db.sync();

app.listen(process.env.PORT, () => {
	var brokers = process.env.BROKERS.split(' ');
    exports.kafka = new Kafka({
        clientId: 'user-authorization',
        brokers: brokers
    });
	exports.producer = exports.kafka.producer();

	console.log(`User-Authorization running on port ${process.env.PORT}`);
});
