const { ORDERING_ENDPOINT, RECOMMENDATION_ENDPOINT } = require('../config/api_endpoint');
const moment = require('moment');
const router = require('express').Router();

moment.locale('id');

router.get('/', async (req, res) => {
	try {
		let {
			data: { data }
		} = await req.axios.get(`${ORDERING_ENDPOINT}/allorder-buyer`);

		data = data.map(el => ({
			...el,
			nama: el.transactions[0].product.nama,
			createdAt: moment(el.createdAt).format('DD MMM YYYY'),
			updatedAt: moment(el.updatedAt).format('DD MMM YYYY'),
			lunas: el.paid === el.total
		}));

		var userId = res.locals.userId;
		if (userId == null)
			userId = -1;
			
		let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
        popular = popular.data.data[0];
		
		res.render('transaction-buyer', { data, popular });
	}
	catch (err) {
		console.error(err);
		res.json({ err });
	}
});

router.get('/done', async (req, res) => {
	try {
		let {
			data: { data }
		} = await req.axios.get(`${ORDERING_ENDPOINT}/allorder-buyer?status=lunas`);

		data = data.map(el => ({
			...el,
			nama: el.transactions[0].product.nama,
			createdAt: moment(el.createdAt).format('DD MMM YYYY'),
			updatedAt: moment(el.updatedAt).format('DD MMM YYYY'),
			lunas: el.paid === el.total,
			rejected: el.transactions[0].processed === 2
		}));

		var userId = res.locals.userId;
		if (userId == null)
			userId = -1;
			
		let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
		popular = popular.data.data[0];
		
		res.render('transaction-buyer', { data, popular });
	}
	catch (err) {
		console.error(err);
		res.json({ err });
	}
})

router.get('/ordered', async (req, res) => {
	try {
		let {
			data: { data }
		} = await req.axios.get(`${ORDERING_ENDPOINT}/allorder-seller`);

		data = data.map(el => ({
			...el,
			createdAt: moment(el.createdAt).format('DD MMM YYYY'),
			updatedAt: moment(el.updatedAt).format('DD MMM YYYY')
		}));

		var userId = res.locals.userId;
		if (userId == null)
			userId = -1;
		
		let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
		popular = popular.data.data[0];

		res.render('transaction-seller', { data, popular, isOrdered: true });
	}
	catch (err) {
		console.error(err);
		res.json({ err });
	}
})

router.get('/processed', async (req, res) => {
	try {
		let {
			data: { data }
		} = await req.axios.get(
			`${ORDERING_ENDPOINT}/allorder-seller?status=processed`
		);

		data = data.map(el => ({
			...el,
			createdAt: moment(el.createdAt).format('DD MMM YYYY'),
			updatedAt: moment(el.updatedAt).format('DD MMM YYYY'),
			rejected: el.processed === 2
		}));

		var userId = res.locals.userId;
		if (userId == null)
			userId = -1;
			
		let popular = await req.axios.get(`${RECOMMENDATION_ENDPOINT}/recommend?userId=${userId}`);
		popular = popular.data.data[0];

		res.render('transaction-seller', { data, popular, isOrdered: false });
	}
	catch (err) {
		console.error(err);
		res.json({ err });
	}
})

router.post('/create', async (req, res) => {
	try {
		console.log(">>>>>>>>>>>>>>>>> <<<<<<<<<<<<<<<<<<<<<");
		console.log(req.body);
		await req.axios.post(ORDERING_ENDPOINT + '/create', {
			items: [{ ...req.body }]
		});

		res.redirect('/transaction');
	}
	catch (err) {
		if (err && err.response) {
			res.redirect('/?error=' + err.response.data.errorMsg);
		}
		else {
			res.json({ error });
		}

		console.error(error);
	}
})

router.get('/process/:id', async (req, res) => {
	try {
		const result = await req.axios.post(ORDERING_ENDPOINT + '/process-order', {
			transactionId: req.params.id
		});

		setTimeout(() => {
			res.redirect('/transaction/processed')
		}, 5000);
	}
	catch (err) {
		res.send(err);
		console.error(err);
	}
})

router.get('/reject/:id', async (req, res) => {
	try {
		await req.axios.post(ORDERING_ENDPOINT + '/reject-order', {
			transactionId: req.params.id
		});

		setTimeout(() => {
			res.redirect('/transaction/processed')
		}, 5000);
	}
	catch (err) {
		res.send(err);
		console.error(err);
	}
})

module.exports = router;
