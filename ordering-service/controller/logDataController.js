const Transaction = require('../models/').Transaction;
const Product = require('../models/').Product;
const User = require('../models/').User;
const Invoice = require('../models/').Invoice;

var exports = module.exports = {};

// exports.getUserTransactions = async (userId, processed=false) => {
// 	const productId = await Product.findAll({
// 		attributes: ['id'],
// 		where: {
// 			userId: userId
// 		}
// 	});

// 	return await Transaction.findAll({
//         attributes: ['id'],
// 		where: {
// 			productId: productId.map(el => el.id),
// 			processed: processed ? {
// 				[Sequelize.Op.gte]: 1
// 			}
// 			: 0
// 		}
// 	});
// }

exports.getProductByTransactionId = async (transactionId) => {
    try {
        var queryResult = await Transaction.findOne({
            attributes: ['productId'],
            where: {
                id: transactionId
            }
        });

        if (queryResult)
            return await exports.getProductById(queryResult.productId);
        else
            return { error: 'Transaction Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getBuyerByTransactionId = async (transactionId) => {
    try {
        var queryResult = await Transaction.findOne({
            attributes: ['id'],
            where: {
                id: transactionId
            },
            include: [{
                model: Invoice,
                attributes: ['buyerId']
            }]
        });

        if (queryResult)
            return await exports.getUserById(queryResult.invoice.buyerId);
        else
            return { error: 'Transaction Not Found' };
    }
    catch (err) {
        console.error(err);
        return { err, isError: true };
    }
}

exports.getTransactionById = async (transactionId) => {
    try {
        var data = await Transaction.findOne({
            attributes: ['processed'],
            where: {
                id: transactionId
            }
        });

        if (data)
            return data.dataValues;
    }
    catch (err) {
        console.error(err);
        return { err, isError: true };
    }
}

exports.getUserByProductId = async (productId) => {
    try {
        const queryResult = await Product.findOne({
            attributes: ['userId'],
            where: {
                id: productId
            }
        });

        if (queryResult)
            return await exports.getUserById(queryResult.userId);
        else
            return { error: 'Product Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getUserById = async (userId) => {
    try {
        const id = Number(userId);
        var data = await User.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}

exports.getProductById = async (productId) => {
    try {
        const id = Number(productId);
        var data = await Product.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'Product Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
}
