require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
const { Kafka } = require('kafkajs');

const db = require('./models/');
const logDataController = require('./controller/logDataController');
const router = require('./controller/transaction');

const app = express();

var exports = module.exports = {};
exports.kafka = null;
exports.producer = null;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * "/reject-order" -> reject requested order
 * "/process-order" -> accept requested order
 * "/allorder-buyer" -> get unpaid orders
 * "/allorder-buyer?status=lunas" -> get paid orders
 * "/allorder-seller" -> get unaccepted orders requests'
 * "/allorder-seller?status=processed" -> get accepted orders requests'
 * "/create" -> create ordergetRandomArbitrary
 */
app.use(morgan(async (tokens, req, res) => {
	var userId = 0;
	const auth = req.headers.authorization.split(' ')[1];
	try {
		const { id } = jwt.verify(auth, process.env.SECRET);
		userId = id;
	} catch (err) {
		console.log("samir cute!!! " + err);
	}

	var method = tokens.method(req, res);
	var url = tokens.url(req, res);
	var response = tokens.res(req, res, 'content-length');
	var status = tokens.status(req, res);

	var time = new Date();
	const timestamp = `${time.getFullYear()}-${time.getMonth()+1}-${time.getDate()} ${time.getHours()+1}:${time.getMinutes()}:${time.getSeconds()}`;
	
	var log = {
		method: method,
		url: url,
		status: status,
		userId: userId,
		productId: 0,
		timestamp: timestamp,
		user: {},
		product: {}
	};

	switch(log.url) {
		case "/allorder-buyer":
			log.event = "getUnpaidOrders";
			break;
		case "/allorder-buyer?status=lunas":
			log.event = "getPaidOrders";
			break;
		case "/allorder-seller":
			log.event = "getUnacceptedOrders";
			break;
		case "/allorder-seller?status=processed":
			log.event = "getAcceptedOrders";
			break;
		case "/create":
			log.event = "createOrder";
			break;
		case "/process-order":
			log.event = "acceptOrder";
			break;
		case "/reject-order":
			log.event = "rejectOrder";
			break;
		default:
			log.event = "unknown";
			break;
	};

	if (log.url === "/process-order" || log.url === "reject-order") {
		log.user = await logDataController.getBuyerByTransactionId(req.body.transactionId);
		log.product = await logDataController.getProductByTransactionId(req.body.transactionId);
		log.productId = Number(log.product.id);
	}

	if (log.url === "/create") {
		var items = req.body.items;
		items = items[0];
		log.user = await logDataController.getUserById(userId);
		log.product = await logDataController.getProductById(items.id);
		log.productId = Number(log.product.id);
	}

	await exports.producer.connect();
    await exports.producer.send({
        topic: process.env.KAFKA_TOPIC,
        messages: [ { value: JSON.stringify(log) } ],
    });

	return [
		method,
		url,
		status,
		response, '-',
		tokens['response-time'](req, res), 'ms'
	].join(' ');
}));

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Method', 'GET, POST, PUT, DELETE');
	next();
});

app.use('/', router);

db.sync();

app.listen(process.env.PORT, () => {
	var brokers = process.env.BROKERS.split(' ');
    exports.kafka = new Kafka({
        clientId: 'ordering-service',
        brokers: brokers
    });
	exports.producer = exports.kafka.producer();
	
	console.log(`Ordering Service running on port ${process.env.PORT}`);
});
