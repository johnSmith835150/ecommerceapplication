require('dotenv').config();
const Sequelize = require('sequelize');
const Product = require('../models').Product;
const User = require('../models').User;
const sequelize = require('../models').sequelize;


var exports = module.exports = {};
exports.usersRecommendations = new Map();

exports.randomProductsSample = async (sampleSize) => {
	try {
		var data = await Product.findAll({ order: [Sequelize.fn('RAND')], limit: sampleSize });

		return [data, data];
	}
	catch (err) {
		return null;
	}
};

exports.recommend = async (req, res) => {
	const userId = Number(req.query.userId);

	var data = await sequelize.query(
		"SELECT COUNT('*') AS total, nama, harga, photoUrl, products.id FROM `transactions`, products WHERE productId=products.id GROUP BY productId ORDER BY total DESC LIMIT 10"
	);
	if (userId == undefined || userId == null || userId < 1 || !exports.usersRecommendations.has(userId)) {
		res.json({ isOk: true, data });
	}
	else {
		try {
			var userData = exports.usersRecommendations.get(userId);
			var recommendedProductsIds = userData.productIds;

			let recommendedProducts = await Product.findAll({
				where: {
					id: recommendedProductsIds
				},
				include: [{
					model: User,
					attributes: ['id', 'nama'],
					where: {
					id: sequelize.col('product.userId')
					},
					as: 'seller'
				}],
				order: [['id', 'DESC'], ['nama', 'ASC']]
			});
	
			data = [recommendedProducts, recommendedProducts];
			res.json({ isOk: true, data });
		}
		catch (err) {
			console.log(err);
			res.status(500).json({ isError: true, err });
		}
	}
};

exports.getUserById = async (userId) => {
    try {
        const id = Number(userId);
        var data = await User.findOne({
            where: {
                id
            },
            attributes: {
                exclude: ['password', 'photoUrl']
            }
        });

        if (data)
            return data.dataValues;
        else
            return { error: 'User Not Found' };
    }
    catch (err) {
        console.error(err);
        return { error: err };
    }
};
