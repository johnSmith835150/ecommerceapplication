require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
const { Kafka } = require('kafkajs');
const axios = require('axios');

const db = require('./models/');
const router = require('./routes/web');
const recommendationController = require('./controllers/recommendationController');

const app = express();

var exports = module.exports = {};
exports.kafka = null;
exports.producer = null;
exports.consumer = null;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

exports.consume = async () => {
    await exports.consumer.connect();
    await exports.consumer.subscribe({ topic: process.env.KAFKA_PREDICT_TOPIC, fromBeginning: true })
   
    await exports.consumer.run({
        eachMessage: async ({ topic, partition, message }) => {
            var parsedMessage = JSON.parse(message.value.toString());

            var userId = parsedMessage.userId;
            if (userId >= 1) {
                var timestamp = parsedMessage.timestamp;
                var recommendations = parsedMessage.productIds;
                
                if(recommendationController.usersRecommendations.has(userId)) {
                    var data = recommendationController.usersRecommendations.get(userId);
    
                    if (data.timestamp < timestamp)
                        data = { "timestamp": timestamp, "productIds": recommendations };
                    
                    recommendationController.usersRecommendations.set(userId, data);
                }
                else {
                    var data = data = { "timestamp": timestamp, "productIds": recommendations };
                    recommendationController.usersRecommendations.set(userId, data);
                }
            }
        }
    });
}

/** 
 * "/recommend" -> get user recommendations
 */
app.use(morgan(async (tokens, req , res) => {
    var userId = 0;
    const auth = req.headers.authorization.split(' ')[1];
    try {
        const { id } = jwt.verify(auth, process.env.SECRET);
        userId = id;
    }
    catch (err) {
        console.log("samir cute!!! " + err);
    }

    var method = tokens.method(req , res);
    var url = tokens.url(req , res);
    var response = tokens.res(req, res, 'content-length');
    var status = tokens.status(req, res);

    var time = new Date();
    const timestamp = `${time.getFullYear()}-${time.getMonth()+1}-${time.getDate()} ${time.getHours()+1}:${time.getMinutes()}:${time.getSeconds()}`;
    
    var log = {
        method: method,
        url: url,
        status: status,
        userId: userId,
        productId: 0,
        timestamp: timestamp,
        user: {},
		product: {}
    };

    if (userId !== 0)
        log.user = await recommendationController.getUserById(userId);

    if (log.url.split("?")[0] === "/recommend")
        log.event = "recommend";
    else
        log.event = "unknown";

    await exports.producer.connect();
    await exports.producer.send({
        topic: process.env.KAFKA_LOG_TOPIC,
        messages: [ { value: JSON.stringify(log) } ],
    });

    return [
        method,
        url,
        status,
        response, '-',
        tokens['response-time'](req, res), 'ms'
    ].join(' ');

}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Method', 'GET, POST, PUT, DELETE')
    req.axios = axios;
    next();
});
app.use('/', router);

db.sync();

app.listen(process.env.PORT, () => {
    var brokers = process.env.BROKERS.split(' ');
    exports.kafka = new Kafka({
        clientId: 'recommendation',
        brokers: brokers
    });
    exports.producer = exports.kafka.producer();
    exports.consumer = exports.kafka.consumer({ groupId: process.env.KAFKA_GROUP });
    exports.consume().catch(e => console.error(`[example/consumer] ${e.message}`, e));

    console.log(`Ordering Service running on port ${process.env.PORT}`);
});
